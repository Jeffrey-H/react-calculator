import { StyleSheet } from 'react-native';

var Style = StyleSheet.create({
    rootContainer: {
        width: "100%",
        flex: 1
    },

    displayContainer: {
        flex: 2,
        backgroundColor: '#193441',
        justifyContent: 'center'
    },

    displayText: {
        color: 'white',
        fontSize: 30,
        textAlign: 'right',
        padding: 10
    },

    inputContainer: {
        flex: 8,
        backgroundColor: '#3E606F'
    },

    inputButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 0.5,
        borderColor: '#91AA9D'
    },

    inputButtonText: {
        fontSize: 22,
        color: 'white'
    },

    inputRow: {
        flex: 1,
        flexDirection: 'row'
    },

    inputButtonHighlighted: {
        backgroundColor: '#193441'
    }
});

export default Style;