import React from 'react';
import {View, Text, TouchableHighlight} from 'react-native';
import Style from './Style';
import InputButton from './InputButton';

const inputButtons = [
    [1, 2, 3, '/'],
    [4, 5, 6, 'x'],
    [7, 8, 9, '-'],
    ['.', 0, '=', '+'],
    ['CE', "M+", "MR", "MC"]
];


export default class App extends React.Component {
    render() {
        return (
                <View style={Style.rootContainer}>
                    <View style={Style.displayContainer}>
                        <Text style={Style.displayText}>{this.state.inputValue}</Text>
                    </View>
                    <View style={Style.inputContainer}>
                        {this._renderInputButtons()}
                    </View>
                </View>
        )
    }

    constructor(props) {
        super(props);

        this.state = {
            previousInputValue: 0,
            inputValue: 0,
            selectSymbol: null,
            memory: 0
        }
    }

    _renderInputButtons() {
        let views = [];

        inputButtons.forEach((value, r) => {
            let row = value;
            let inputrow = [];
            row.forEach((v2, i) => {
                if(v2 === '='){
                    inputrow.push(<InputButton style={{width: '50%'}} value={v2} highlight={this.state.selectedSymbol === v2} onPress={this._onInputButtonPressed.bind(this, v2)} key={r + "-" + i}/>);
                }else{
                    inputrow.push(<InputButton value={v2} highlight={this.state.selectedSymbol === v2} onPress={this._onInputButtonPressed.bind(this, v2)} key={r + "-" + i}/>);
                }

            });
            views.push(<View style={Style.inputRow} key={"row-" + r}>{inputrow}</View>)
        });

        return views;
    }

    _onInputButtonPressed(input) {
        switch (typeof input) {
            case 'number':
                return this._handleNumberInput(input)
            case 'string':
                return this._handleStringInput(input)
        }
    }

    _handleNumberInput(num) {
        let inputValue = (this.state.inputValue * 10) + num;

        this.setState({
            inputValue: inputValue
        })
    }

    _handleStringInput(str) {
        switch (str) {
            case '/':
            case 'x':
                this.setState({
                    selectedSymbol: '*',
                    previousInputValue: this.state.inputValue,
                    inputValue: 0
                });
                break;
            case '+':
            case '-':
                this.setState({
                    selectedSymbol: str,
                    previousInputValue: this.state.inputValue,
                    inputValue: 0
                });
                break;

            case '=':
                let symbol = this.state.selectedSymbol,
                    inputValue = this.state.inputValue,
                    previousInputValue = this.state.previousInputValue;

                if (!symbol) {
                    return;
                }

                this.setState({
                    previousInputValue: 0,
                    inputValue: eval(previousInputValue + symbol + inputValue),
                    selectedSymbol: null
                });
                break;
            case "CE":
                this.setState({
                    inputValue: 0
                });
                break;
            case 'M+':
                const mem = this.state.memory + this.state.inputValue;
                this.setState({
                    memory: mem
                });
                break;
            case 'MR':
                this.setState({
                    inputValue: this.state.memory
                });
                break;
            case 'MC':
                this.setState({
                    memory: 0
                });
                break;
        }
    }

}